# rnkizw

Ein kleiner Helfer um eine bessere Übersicht über freier Termine in den Impfzentren de Rhein-Neckar-Kreises zu bekommen.

## Kommandozeile

Um auf der Kommandozeile zu prüfen, ob es aktuell freie Termine gibt, einfach das Skript `rnkizw` oder das gleichnamige Modul ausführen.

    >>> rnkizw
    Freie Termine in Impfung Bammental - Vertusplatz 1, 69245 Bammental: BioNTech (1), Moderna (1), Johnson & Johnson (1)

    Besuche https://c19.rhein-neckar-kreis.de/impftermin um einen Termin zu buchen.

Alternativ:

    >>> python3 -m rnkizw
    Es wurden leider keine freien Termine gefunden. 😞

Mithilfe des parameters `--mail-to` kann eine Emailadresse angegeben werden, welche über freie Termine benachrichtigt wird.
Dies ist nützlich, um das Skript beispielsweise stündlich im Hintergrund auszuführen.
Mit `--always-mail`  wird auch eine Mail verschickt, falls keine Termine verfügbar sind.

## Web

Die Impftermine können auch als Dashboard dargestellt werden.
Es handelt sich um eine Django-App die wie jede andere Django-App ausgeführt werden sollte.
Die WSGI-App ist `rnkizw.wsgi:application`.
Die ASGI-App ist `rnkizw.asgi:application`.
Außerdem muss regelmäßig `manage.py aktualisieren` ausgeführt werden um die Daten des Dashboards zu aktualisieren.

Zur Konfiguration sollten folgende Umgebungsvariablen gesetzt werden:
* `RNKIZW_SECRECT`: Eine zufällige, geheime Zeichenkette. Für Details bitte die Doku von Django lesen.
* `RNKIZW_ALLOWED_HOSTS`: Eine durch Semikola separierte Liste von Hostnamen über die auf das Dashboard zugegriffen werden darf. Per default `localhost`.
* `RNKIZW_IMPRESSUM_POSTADRESSE`: Postadresse der verantwortlichen Person für Impressum und Datenschutzerklärung.
* `RNKIZW_IMPRESSUM_EMAIL`: E-Mail-Adresse der verantwortlichen Person für Impressum und Datenschutzerklärung.
* `RNKIZW_DB_ENGINE`: Erlaubt eine andere Datenbank als SQLite zu nutzen. Für PostgreSQL muss `django.db.backends.postgresql` ausgewählt werden. In diesem Fall müssen auch mindestens der Name und das Passwort konfiguriert werden. Dann wird von einer lokalen PostgreSQL ausgegangen.
* `RNKIZW_DB_NAME`, `RNKIZW_DB_USER`, `RNKIZW_DB_PASSWORD`, `RNKIZW_DB_HOST`, `RNKIZW_DB_PORT`: Erlauben die Anpassung der zu nutzenden Datenbank.
* `RNKIZW_STATIC_URL`: URL von der Browser die statischen Inhalte abrufen sollen.
* `RNKIZW_STATIC_ROOT`: Pfad in den die statischen Inhalte beim Aufruf von `collectstatic` kopiert werden sollen.
* `RNKIZW_ADMINS`: Liste von Empfängern für Fehlerbenachrichtigungen. Das Format ist: `Erika Mustermann,erika.mustermann@example.org;Max Mustermann,max.mustermann@example.org`, also Semikola um Empfänger zu trennen, und ein Komma um den Empfängernamen von der E-Mail-Adresse zu trennen.
* `RNKIZW_MAIL_USER`: Nutzer für den Versand von E-Mails inklusive Domain.
* `RNKIZW_MAIL_PASSWORD`: Passwort für den Versand von E-Mails.
* `RNKIZW_MAIL_HOST`: SMTP-Host über den E-Mails verschickt werden sollen.

## Lizenz

Dieser kleine Helfer ist unter der AGPL-3 oder später lizenziert.
Die komplette Lizenz ist in der Datei [`LICENSE`](./LICENSE) zu finden.
