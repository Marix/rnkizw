import pytest
from django.db import IntegrityError

from rnkizw.dashboard.models import Impfstoff, Impfzentrum, Verfügbarkeit


@pytest.mark.django_db
def test_keine_doppelungen():
    z1 = Impfzentrum(1, 'Eins', 'Ort 1')
    s1 = Impfstoff(1, 'Eins')

    Verfügbarkeit.objects.create(impfzentrum=z1, impfstoff=s1, menge=1)
    with pytest.raises(IntegrityError):
        Verfügbarkeit.objects.create(impfzentrum=z1, impfstoff=s1, menge=1)


@pytest.mark.django_db
def test_aktualisiert_auch_ohne_änderungen():
    z1 = Impfzentrum(1, 'Eins', 'Ort 1')
    z1.save()
    s1 = Impfstoff(1, 'Eins')
    s1.save()

    obj1 = Verfügbarkeit.objects.create(impfzentrum=z1, impfstoff=s1, menge=1)
    obj2, erstellt = Verfügbarkeit.objects.update_or_create(impfzentrum=z1, impfstoff=s1, defaults={
        'menge': 1,
    })
    assert not erstellt
    assert obj2.aktualisiert > obj1.aktualisiert
