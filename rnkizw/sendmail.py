import subprocess  # nosec
from email.message import EmailMessage


def send_mail(to: str, subject: str, body: str) -> None:
    msg = EmailMessage()
    msg.set_content(body)
    msg['To'] = to
    msg['Subject'] = subject

    # External input is only passed into the body and even that is sent through the email
    # module which should ensure stuff is properly encoded.
    subprocess.run(["/usr/sbin/sendmail", "-t", "-oi"], input=msg.as_bytes())  # nosec
