from django.db import models


class Impfzentrum(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=64)
    adresse = models.CharField(max_length=256)

    def __str__(self):
        return self.name


class Impfstoff(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=256)

    def __str__(self):
        return self.name


class Verfügbarkeit(models.Model):
    impfzentrum = models.ForeignKey(Impfzentrum, on_delete=models.CASCADE)
    impfstoff = models.ForeignKey(Impfstoff, on_delete=models.CASCADE)
    menge = models.IntegerField(default=0)
    aktualisiert = models.DateTimeField('Letzte Aktualisierung', auto_now=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['impfzentrum', 'impfstoff'], name='unique_verfügbarkeit')
        ]

    def __str__(self):
        return f'{self.menge} {self.impfstoff} in {self.impfzentrum} gesehen am {self.aktualisiert:%Y-%m-%d um %H:%M}'
