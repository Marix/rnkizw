from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render

from rnkizw import settings
from rnkizw.dashboard.models import Impfstoff, Impfzentrum


def index(request):
    impfstoffe = Impfstoff.objects.all().order_by('id')
    impfzentren = Impfzentrum.objects.all().order_by('id')

    zeilen = []
    for impfzentrum in impfzentren:
        mengen = []
        for impfstoff in impfstoffe:
            try:
                verfügbarkeit = impfzentrum.verfügbarkeit_set.get(impfstoff=impfstoff)
            except ObjectDoesNotExist:
                mengen.append(0)
            else:
                mengen.append(verfügbarkeit.menge)

        max_termine = max(menge for menge in mengen)
        verfügbarkeiten = impfzentrum.verfügbarkeit_set.all()

        zeilen.append({
            'impfzentrum': impfzentrum,
            'mengen': mengen,
            'status': 'freie_termine' if max_termine >= 5 else 'wenige_termine' if max_termine > 0 else 'keine_termine',
            'aktualisiert': min(
                verfügbarkeit.aktualisiert for verfügbarkeit in impfzentrum.verfügbarkeit_set.all()
            ) if verfügbarkeiten else '',
        })

    return render(request, 'dashboard/index.html', {
        'impfstoffe': impfstoffe,
        'zeilen': zeilen,
    })


def impressum_und_datenschutz(request):
    return render(request, 'dashboard/impressum_und_datenschutz.html', {
        'postadresse': settings.IMPRESSUM_POSTADRESSE,
        'email': settings.IMPRESSUM_EMAILADRESSE,
    })
