from django.contrib import admin

from .models import Impfstoff, Impfzentrum, Verfügbarkeit

admin.site.register(Impfzentrum)
admin.site.register(Impfstoff)
admin.site.register(Verfügbarkeit)
