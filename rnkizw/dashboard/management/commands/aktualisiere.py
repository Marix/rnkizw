from django.core.management.base import BaseCommand

from ....scraper import create_session, lade_anzahl_freier_termine, lade_impfstoffe, lade_orte
from ...models import Impfstoff, Impfzentrum, Verfügbarkeit


class Command(BaseCommand):
    help = 'Aktualisiere die Verfügbarkeitsinformationen'

    def handle(self, *args, **options):
        session = create_session()

        for ort in lade_orte(session):
            impfzentrum, _ = Impfzentrum.objects.get_or_create(id=ort.id, defaults={
                'name': ort.name,
                'adresse': ort.adresse,
            })

            for scraped_impfstoff in lade_impfstoffe(session, ort):
                impfstoff, _ = Impfstoff.objects.get_or_create(id=scraped_impfstoff.id, defaults={
                    'name': scraped_impfstoff.name
                })

                anzahl_freier_termine = lade_anzahl_freier_termine(session, ort, impfstoff)
                Verfügbarkeit.objects.update_or_create(
                    impfzentrum=impfzentrum,
                    impfstoff=impfstoff,
                    defaults={
                        'menge': anzahl_freier_termine,
                    }
                )
