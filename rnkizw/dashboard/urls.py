from django.urls import path

from . import views

app_name = 'dashboard'
urlpatterns = [
    path('', views.index, name='startseite'),
    path('impressum_und_datenschutz/', views.impressum_und_datenschutz, name='impressum_und_datenschutz')
]
