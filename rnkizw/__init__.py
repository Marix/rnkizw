from typing import Optional

import typer

from .scraper import suche_freie_termine
from .sendmail import send_mail

app = typer.Typer()


@app.command()
def main(
        mail_to: Optional[str] = typer.Option(None, help='Benachrichtige diese Emailadresse über freie Impftermine.'),
        always_mail: bool = typer.Option(False, help='Sende auch wenn keine Termine gefunden wurden eine Email.'),
):
    """
    Suche nach freien Terminen bei den Impfzentren des Rhein-Neckar-Kreises.
    """
    freie_termine = suche_freie_termine()

    if freie_termine:
        msg = '\n'.join(
            f'''Freie Termine in {ort.name}: {", ".join(
                f"{impfstoff.name} ({termine})" for impfstoff, termine in impfstoffe
            )}'''
            for ort, impfstoffe in freie_termine.items()
        )
        msg += '\n\nBesuche https://c19.rhein-neckar-kreis.de/impftermin um einen Termin zu buchen.'
        typer.echo(msg)
        if mail_to:
            send_mail(mail_to, 'Freie Impftermine! 🎉', msg)
    else:
        msg = 'Es wurden leider keine freien Termine gefunden. 😞'
        typer.echo(msg)
        if mail_to and always_mail:
            send_mail(mail_to, 'Keine Impftermine 😞', msg)
