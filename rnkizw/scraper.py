from collections import defaultdict
from typing import Dict, List, Tuple

from requests import Session
from requests.adapters import HTTPAdapter
from urllib3 import Retry

from .model import Impfstoff, Ort

REQUEST_TIMEOUT = 5.


def create_session() -> Session:
    retry_strategy = Retry(
        status_forcelist=[429, 500, 502, 503, 504],
        backoff_factor=1,
    )
    adapter = HTTPAdapter(max_retries=retry_strategy)
    session = Session()
    session.mount("https://", adapter)
    session.mount("http://", adapter)
    return session


def lade_orte(session: Session) -> List[Ort]:
    orte = session.post(
        'https://c19.rhein-neckar-kreis.de/public/getTeststationenTicketService',
        data={
            'type': 2,
            'h': 0,
        },
        timeout=REQUEST_TIMEOUT,
    )
    orte.raise_for_status()

    return [Ort(ort['Id'], ort['Name'], ort['Adresse']) for ort in orte.json()['items']]


def lade_impfstoffe(session: Session, ort: Ort) -> List[Impfstoff]:
    impfstoffe = session.post(
        'https://c19.rhein-neckar-kreis.de/data/getSelfServiceVaccines',
        data={
            'teststationId': ort.id
        },
        timeout=REQUEST_TIMEOUT,
    )
    impfstoffe.raise_for_status()
    return [Impfstoff(impfstoff['Id'], impfstoff['Kurzbezeichnung']) for impfstoff in impfstoffe.json()['items']]


def lade_anzahl_freier_termine(session: Session, ort: Ort, impfstoff: Impfstoff):
    tage = session.post(
        'https://c19.rhein-neckar-kreis.de/data/getFreeDates',
        data={
            'teststationId': ort.id,
            'vaccineId': impfstoff.id,
            'selfService': 'true',
        },
        timeout=REQUEST_TIMEOUT,
    )
    tage.raise_for_status()

    tage_mit_terminen = [item['date'] for item in tage.json()['items']]

    termine = 0

    for tag in tage_mit_terminen:
        zeiten = session.post(
            'https://c19.rhein-neckar-kreis.de/data/getFreeTimes',
            data={
                'teststationId': ort.id,
                'date': tag,
                'selfService': 'true',
            },
            timeout=REQUEST_TIMEOUT,
        )
        zeiten.raise_for_status()
        termine += len(zeiten.json()['items'])  # Die liste sieht aus wie `['08:10']`.

    return termine


def suche_freie_termine() -> Dict[Ort, List[Tuple[Impfstoff, int]]]:
    session = create_session()

    orte = lade_orte(session)

    freie_termine = defaultdict(list)

    for ort in orte:
        for impfstoff in lade_impfstoffe(session, ort):
            anzahl_freier_termine = lade_anzahl_freier_termine(session, ort, impfstoff)
            if anzahl_freier_termine:
                freie_termine[ort].append((impfstoff, anzahl_freier_termine))

    return freie_termine
