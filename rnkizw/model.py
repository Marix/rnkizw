from attr import dataclass


@dataclass(frozen=True)
class Ort:
    id: str
    name: str
    adresse: str


@dataclass(frozen=True)
class Impfstoff:
    id: str
    name: str
